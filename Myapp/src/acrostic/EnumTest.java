package acrostic;

public class EnumTest {
    enum Season {
        FALL, SPRING, SUMMER, WINTER
    }
    enum Season1 {
        FALL(1), SPRING(2), SUMMER(3), WINTER(4);
        private int code;

        public static Season1 valueOf(int code) {
            for(Season1 season1:Season1.values())
                if(season1.getCode()==code) return season1;
            throw  new RuntimeException("Not found");
        }

        public int getCode() {
            return code;
        }

        Season1(int code) {
            this.code = code;
        }
    }


    public static void main(String[] args) {
        Season season = Season.valueOf("FALL");
        System.out.println(season.compareTo(Season.FALL));
        season = Season.WINTER;
        System.out.println("WINTER".equals(season.name()));

        for(Season season1:Season.values())
            System.out.println(season1.name());

        Season1 season1 = Season1.WINTER;
        System.out.println(season1.getCode());
        season1 = Season1.valueOf(4);
        System.out.println(season1.name());
        
    }
}
