package acrostic.builder;

public class BankAccount {
    public static class Builder{
        private long accountNumber; //This is important, so we'll pass it to the constructor.
        private String owner;
        private String branch;
        private double balance;
        private double interestRate;
        public Builder(long accountNumber){
            this.accountNumber = accountNumber;
        }
        public Builder withOwner(String owner){
            this.owner = owner;
            return this;
        }
        public Builder onBranch(String branch){
            this.branch = branch;
            return this;
        }
        public Builder openingBalance(double balance){
            this.balance = balance;
            return this;
        }
        public Builder onRate(double interestRate){
            this.interestRate = interestRate;
            return this;
        }
        public BankAccount build(){
            return new BankAccount();
        }
    }
    private BankAccount(){
    }
}
