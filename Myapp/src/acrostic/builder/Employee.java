package acrostic.builder;

public class Employee {
    private final String name;
    private final int age;
    private final int id;
    private final String phone;
    private final String add;

    public static class EmployeeBuilder{
        private final String name;
        private final int age;
        private final int id;
        private  String phone;
        private  String add;
        public EmployeeBuilder(String name, int age, int id){
            this.name = name;
            this.age = age;
            this.id = id;
        }
        public EmployeeBuilder hasPhone(String phone){
            this.phone = phone;
            return this;
        }
        public EmployeeBuilder hasAdd(String add){
            this.add = add;
            return this;
        }
        public Employee build(){
            return new Employee(this);
        }
    }
    private Employee(EmployeeBuilder builder){
        this.name = builder.name;
        this.age = builder.age;
        this.id = builder.id;
        this.phone = builder.phone;
        this.add = builder.add;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getAdd() {
        return add;
    }
}
