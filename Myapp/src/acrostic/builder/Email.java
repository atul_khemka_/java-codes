package acrostic.builder;
// to test fluent builder patterns
public class Email {
    String to;
    String from;
    String subject;
    String content;
    String cc;
    String bcc;

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getCc() {
        return cc;
    }

    public String getBcc() {
        return bcc;
    }

    @Override
    public String toString() {
        return "Email{" +
                "to='" + to + '\'' +
                ", from='" + from + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                '}';
    }

    private Email(String to, String from, String subject, String content, String cc, String bcc) {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.content = content;
        this.cc = cc;
        this.bcc = bcc;
    }

    public interface EmailFrom{
        EmailTo setFrom(String from);
    }
    public interface EmailTo{
        EmailSubject setTo(String to);
    }
    public interface EmailSubject{
        EmailContent setSub(String subject);
    }
    public interface EmailContent{
        EmailCreator setCon(String content);
    }
    public interface EmailCreator{
        EmailCreator setBcc(String bcc);
        EmailCreator setCc(String cc);
        Email build();
    }

    public static class EmailBuilder implements EmailFrom,EmailTo,EmailSubject,EmailContent,EmailCreator{
        String to;
        String from;
        String subject;
        String content;
        String cc;
        String bcc;

        private EmailBuilder(){
        }
        public static EmailFrom getInstance(){
            return  new EmailBuilder();
        }

        @Override
        public EmailTo setFrom(String from) {
            this.from = from;
            return this;
        }

        @Override
        public EmailSubject setTo(String to) {
            this.to = to;
            return this;
        }

        @Override
        public EmailContent setSub(String subject) {
            this.subject = subject;
            return this;
        }

        @Override
        public EmailCreator setCon(String content) {
            this.content = content;
            return this;
        }

        @Override
        public EmailCreator setBcc(String bcc) {
            this.bcc = bcc;
            return this;
        }

        @Override
        public EmailCreator setCc(String cc) {
            this.cc = cc;
            return this;
        }

        @Override
        public Email build() {
            return new Email(to,from,subject,content,cc,bcc);
        }
    }
}
