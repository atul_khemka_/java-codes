package acrostic.Socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GreetClient {
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String host, int port){
        try {
            socket = new Socket(host, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String sendMessage(String msg){
        out.println(msg);
        String res="";
        try {
            res = in.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void stopConnect(){
        out.close();
        try {
            in.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
