package acrostic.Socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NIOSocketClient {
    AsynchronousSocketChannel client;
    public NIOSocketClient() {
        try {
            client = AsynchronousSocketChannel.open();
            InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", 8080);
            Future<Void> future = client.connect(inetSocketAddress);
            future.get();
        }
        catch (IOException | ExecutionException | InterruptedException e){
            e.printStackTrace();
        }
    }
    public String sendMsg(String msg) throws ExecutionException, InterruptedException {
        byte[] byteMsg = msg.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(byteMsg);
        Future<Integer> writeResult = client.write(byteBuffer);
        writeResult.get();
        byteBuffer.flip();
        Future<Integer> readResult = client.read(byteBuffer);
        readResult.get();
        String echo = new String(byteBuffer.array()).trim();
        System.out.println(echo);
        byteBuffer.clear();
        return echo;
    }
}
