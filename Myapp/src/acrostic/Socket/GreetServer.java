package acrostic.Socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class GreetServer {
    private ServerSocket serverSocket;
    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port){

        try {
            serverSocket = new ServerSocket();
            socket = serverSocket.accept();
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String greeting = in.readLine();
            if("hello server".equals(greeting))
                out.println("hello client");
            else
                out.println("unrecognized greeting");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void stop() {
        try {
            in.close();
            out.close();
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GreetServer greetServer = new GreetServer();
        greetServer.start(6666);
    }

}
