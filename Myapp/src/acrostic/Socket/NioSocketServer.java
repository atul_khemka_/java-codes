package acrostic.Socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class NioSocketServer {
    AsynchronousSocketChannel clientChannel;
    public NioSocketServer() {
        try {
            //since java 1.7, earlier 1.4 only ServerSocketChannel
            final AsynchronousServerSocketChannel listner = AsynchronousServerSocketChannel.open()
                    .bind(new InetSocketAddress(5000));

            // can also be implemented using Future and its associated methods
            listner.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>(){

                @Override
                public void completed(AsynchronousSocketChannel ch, Void attachment) {
                    clientChannel = ch;
                    listner.accept(null,this);
                    ch.write(ByteBuffer.wrap("hello from echo server\n".getBytes()));
                    ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
                    try {
                        // these can be replaced by readwritehandler class
                        int bytesRead = ch.read(byteBuffer).get(20, TimeUnit.SECONDS);
                        boolean running = true;
                        while (bytesRead != -1 && running){
                            System.out.println("bytes read: "+ bytesRead);
                            if(byteBuffer.position() > 2){
                                byteBuffer.flip();
                                byte[] lineBytes = new byte[bytesRead];
                                byteBuffer.get(lineBytes,0,bytesRead);
                                String line = new String(lineBytes);
                                System.out.println("msg: "+ line);
                                ch.write(ByteBuffer.wrap(line.getBytes()));
                                byteBuffer.clear();
                                bytesRead = ch.read(byteBuffer).get(20, TimeUnit.SECONDS);
                            }
                            else
                                running = false;
                        }
                    }
                    catch (InterruptedException | ExecutionException e){
                        e.printStackTrace();
                    }
                    catch (TimeoutException e){
                        ch.write(ByteBuffer.wrap("Good Bye\n".getBytes()));
                        System.out.println( "Connection timed out, closing connection" );
                    }
                    try {
                        if(ch.isOpen()) ch.close();
                    }
                    catch (IOException e1){
                        e1.printStackTrace();
                    }
                }

                @Override
                public void failed(Throwable exc, Void attachment) {

                }
            });
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    //  separate inner class for handling read and write operations; ReadWriteHandler.
    //  We will see how the attachment object comes in handy at this point
    class ReadWriteHandler implements CompletionHandler<Integer, Map<String, Object>>{

        @Override
        public void completed(Integer result, Map<String, Object> attachment) {
            String action = (String) attachment.get("action");
            if("read".equals(action)){
                ByteBuffer buffer = (ByteBuffer) attachment.get("buffer");
                buffer.flip();
                attachment.put("action","write");
                clientChannel.write(buffer, attachment, this);
                buffer.clear();
            }
            else if("write".equals(action)){
                ByteBuffer byteBuffer = ByteBuffer.allocate(32);
                attachment.put("action", "read");
                attachment.put("buffer", byteBuffer);
                clientChannel.read(byteBuffer, attachment, this);
            }
        }

        @Override
        public void failed(Throwable exc, Map<String, Object> attachment) {

        }
    }

    public static void main(String[] args) {
        NioSocketServer server = new NioSocketServer();
        try {
            Thread.sleep(60000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
