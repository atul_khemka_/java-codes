package acrostic.Socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class SimpleClientExample {
    public static void main(String[] args) {
        if(args.length < 2){
            System.out.println( "Usage: SimpleClientExample <server> <path>" );
            System.exit( 0 );
        }
        String server = args[0];
        String path = args[1];
        System.out.println( "Loading contents of URL: " + server );
        try(Socket socket = new Socket(server, 80);
            PrintStream out = new PrintStream(socket.getOutputStream());
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));){

            out.println( "GET " + path + " HTTP/1.0" );
            out.println();
            String line;
            while ((line = in.readLine())!= null)
                System.out.println(line);
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
}
