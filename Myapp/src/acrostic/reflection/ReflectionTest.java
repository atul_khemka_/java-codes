package acrostic.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Test{
    private String s;
    public Test(){
        s = "xyz";
    }
    public void method1(){
        System.out.println("in method 1 s = "+ s);
    }
    public void method2(int x, String z){
        System.out.println("in method 2 x ="+ x + z);
    }
    private void method3(){
        System.out.println("in method 3");
    }
}

public class ReflectionTest {
    public static void main(String[] args) throws Exception {
        Test test = new Test();
        Class cls = test.getClass();
        System.out.println("class name is "+ cls.getName());
        Constructor constructor = cls.getConstructor();
        System.out.println("constructor name :"+ constructor.getName());
        System.out.println("public methods :");
        Method[] methods = cls.getMethods();
        for(Method method:methods)
            System.out.println(method.getName());

        Method call1 = cls.getDeclaredMethod("method2", int.class, String.class);
        call1.invoke(test,5,"sdf");
        Field field = cls.getDeclaredField("s");
        field.setAccessible(true);
        field.set(test,"Atul");

        Method call2 = cls.getDeclaredMethod("method1");
        call2.invoke(test);

        Method call3 = cls.getDeclaredMethod("method3");
        call3.setAccessible(true);
        call3.invoke(test);
    }
}
