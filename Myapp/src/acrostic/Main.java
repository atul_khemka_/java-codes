/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acrostic;

import acrostic.builder.BankAccount;
import acrostic.builder.Email;
import acrostic.builder.Employee;

import java.util.*;

/**
 *
 * @author sudhanshu
 */
public class Main {
    
    public static void main(String args[])   {
       /* int arr[] = {12, 11, 13, 5, 6, 7};
  
        System.out.println("Given Array"); 
        printArray(arr); 
        mergesort(arr, 0, arr.length - 1);
  
        System.out.println("\nSorted array"); 
        printArray(arr);
        /*TestService test = new TestService();
        test.serialize();
        test.deSerialize();*/
        Map<String,String> bv = new HashMap<>();
        Set<Integer> sd = new TreeSet<>();

         Integer x = 12;
        x = x+12;
       
        System.out.println(x);
        Employee employee = new Employee.EmployeeBuilder("atul",24,1)
                .hasPhone("123123")
                .hasAdd("kandiwali")
                .build();

        System.out.println(employee.getAge());
        BankAccount account = new BankAccount.Builder(123L).onRate(2.5).openingBalance(23123).build();
        System.out.println(account);
        Email email =  Email.EmailBuilder.getInstance().setFrom("Test@gmail.com").setTo("mail@gmail.com")
                .setSub("Test with only required Fields").setCon(" Required Field Test").build();
        System.out.println(email);

    } 
  static  void mergesort(int[] arr,int l , int r){
        if(r>l){
            int mid = (r + l)/2;
            mergesort(arr, l, mid);
            mergesort(arr, mid+1, r);
            merge(arr,l,mid,r);
        }
    }
   static void merge(int[] arr,int l,int m , int r){
        int[] left = Arrays.copyOfRange(arr, l, m+1);
           // printArray(left);
        int[] right = Arrays.copyOfRange(arr, m+1, r+1);
           // printArray(right);
        int n1 = m - l + 1; 
        int n2 = r - m; 
  
        /* Create temp arrays */
        /*int left[] = new int [n1]; 
        int right[] = new int [n2]; 
  
        
        for (int i=0; i<n1; i++) 
            left[i] = arr[l + i]; 
        for (int j=0; j<n2; j++) 
            right[j] = arr[m + 1+ j]; */
  

        
        int i =0,j=0,k=l;
        while(i<left.length && j<right.length){
            if(left[i]<=right[j]){
                arr[k]=left[i++];
            }
            else{
                arr[k] = right[j++];
            }
            k++;
        }
        while(i<left.length){
            arr[k++]= left[i++];
        }
        while(j<right.length){
            arr[k++]= right[j++];
        }
    }
static void printArray(int[] arr){
    for(int i=0;i<arr.length;i++)
        System.out.println(arr[i]);

}
    }


