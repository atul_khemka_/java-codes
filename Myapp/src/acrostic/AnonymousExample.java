package acrostic;

class Test{
    public void show(){
        System.out.println("hi there");
    }
}

interface AnonymousTest{
    void display();
}

public class AnonymousExample {
    public static void main(String[] args) {

        Test obj = new Test() {  //anonymous class without any name to change its method implementation

            @Override
            public void show() {
                System.out.println("hello there");
            }
        };
        obj.show();

        AnonymousTest test = new AnonymousTest() {
            @Override
            public void display() {
                System.out.println("to use methods of interface, use anonymous class to avoid implanting interface");
            }
        };
        test.display();
    }
}
