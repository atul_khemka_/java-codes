package acrostic;

import java.lang.reflect.InvocationTargetException;

public class ObjectCreation {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Emp emp1 = new Emp();
        Emp emp2 = Emp.class.newInstance();
        Emp emp3 = Emp.class.getConstructor().newInstance();
        Emp emp4 = (Emp) emp1.clone();
        //using deserialization
    }
}

class Emp{
    String name;

    public Emp() {
    }
    @Override
    public Object clone() {

        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return obj;
    }
}