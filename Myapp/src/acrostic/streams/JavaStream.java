package acrostic.streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class JavaStream {
    public static void main(String[] args) throws IOException {
        IntStream
                .range(1,10)
                .forEach(System.out::print);
        System.out.println();

        IntStream
                .range(1,10)
                .skip(5)
                .forEach(x -> System.out.println(x));
        System.out.println();

        System.out.println(
                IntStream
                .range(1,6)
                .sum());

        Stream.of("atul","abhay","ank")
                .sorted()
                .findFirst()
                .ifPresent(System.out::print);
        System.out.println();

        String[] names = {"as","sdfads","wesdf","sadsas","ffd","sdfgd"};
        Arrays.stream(names)
                .filter(x -> x.startsWith("s"))
                .sorted()
                .forEach(System.out::println);

        Arrays.stream(new int[] {2,4,6,8,10})
                .map(x -> x*x)
                .average()
                .ifPresent(System.out::println);

        List<String> name = Arrays.asList("as","sdfads","wesdf","sadsas","ffd","sdfgd");
        name.stream()
                .map(String::toUpperCase)
                .filter(x -> x.startsWith("A"))
                .forEach(System.out::println);
        System.out.println();

        Stream<String> band = Files.lines(Paths.get("src/acrostic/streams/bands.txt"));
        band
            .sorted()
            .filter(x -> x.length() > 12)
            .forEach(System.out::println);
        band.close();
        System.out.println();

        List<String> band2 = Files.lines(Paths.get("src/acrostic/streams/bands.txt"))
                .filter(x -> x.contains("on"))
                .collect(Collectors.toList());
        band2.forEach(System.out::println);
        System.out.println();

        Stream<String> row = Files.lines(Paths.get("src/acrostic/streams/data.txt"));
        int rowcount = (int)row
            .map(x -> x.split(","))
            .filter(x -> x.length == 3)
            .count();
        System.out.println(rowcount);
        row.close();
        System.out.println();

        Stream<String> row1 = Files.lines(Paths.get("src/acrostic/streams/data.txt"));
        row1
            .map(x -> x.split(","))
            .filter(x -> x.length == 3)
            .filter(x -> Integer.parseInt(x[1]) >15)
            .forEach(x -> System.out.println(x[0]+" "+x[1]+" "+x[2]));
        row1.close();
        System.out.println();

        Stream<String> row2 = Files.lines(Paths.get("src/acrostic/streams/data.txt"));
        Map<String,Integer> hmap;
        hmap = row2
                .map(x -> x.split(","))
                .filter(x -> x.length == 3)
                .filter(x -> Integer.parseInt(x[1]) >15)
                .collect(Collectors.toMap(
                        x -> x[0],
                        x ->Integer.parseInt(x[1])
                ));
        row2.close();
        for(String key : hmap.keySet())
            System.out.println(key+" "+hmap.get(key));
        System.out.println();

        double total = Stream.of(7.3,1.4,6.8)
                .reduce(0.0, Double::sum);
        System.out.println(total);

        IntSummaryStatistics intSummaryStatistics = IntStream.of(4,56,6,32,9,2).summaryStatistics();
        System.out.println(intSummaryStatistics);
    }
}
