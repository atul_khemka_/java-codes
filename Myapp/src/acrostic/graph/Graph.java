package acrostic.graph;

import java.util.LinkedList;

public class Graph {
    private int V;
        private LinkedList<Integer> adj[];

    public Graph(int v) {
        V = v;
        adj = new LinkedList[v];
        for (int i=0; i < v; i++)
            adj[i] = new LinkedList();
    }

    void addEdge(int v, int u){
        adj[v].add(u);
    }

    void BFS(int v){
        boolean[] visited = new boolean[V];
        LinkedList<Integer> queue = new LinkedList<>();
        visited[v] = true;
        queue.add(v);
        while (queue.size() != 0){
            v = queue.poll();
            System.out.print(v+" ");
            for (int u : adj[v]){
                if (!visited[u]){
                    visited[u] = true;
                    queue.add(u);
                }
            }

        }
    }
    void DFS(int v){
        boolean[] visited = new boolean[V];
        DFSutil(v,visited);
    }

    void DFSutil(int v, boolean[] visited){
        visited[v] = true;
        System.out.print(v+" ");
        for (int u : adj[v]){
            if (!visited[u])
                DFSutil(u,visited);
        }
    }

    int findMother(){
        boolean[] visited = new boolean[V];
        int m = 0;
        for (int i=0;i<V;i++){
            if (!visited[i]) {
                DFSutil(i, visited);
                m = i;
            }
        }
        visited = new boolean[V];
        DFSutil(m, visited);
        for (boolean x : visited)
            if(!x)
                return -1;
        return m;
    }

    public static void main(String[] args) {
        Graph g = new Graph(7);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 3);
        g.addEdge(4, 1);
        g.addEdge(6, 4);
        g.addEdge(5, 6);
        g.addEdge(5, 2);
        g.addEdge(6, 0);
        System.out.println("BFS of graph from vertex 2 ");
        g.BFS(5);
        System.out.println();
        System.out.println("DFS of graph ");
        g.DFS(2);
        System.out.println();
        System.out.println("mother vertex is "+ g.findMother());
    }
}
