package acrostic.graph;

public class Island {
    static final int ROW = 5, COL = 5;

    void DFS(int[][] m, int row, int col, boolean[][] visited){
        int[] rowNbr = new int[]{-1, -1, -1, 0, 0, 1, 1, 1 };
        int[] colNbr = new int[]{-1, 0, 1, -1, 1, -1, 0, 1 };
        visited[row][col] = true;
        for (int k =0; k<8; k++){
            if(isSafe(m,row+rowNbr[k],col+colNbr[k],visited))
                DFS(m,row+rowNbr[k],col+colNbr[k],visited);
        }
    }

    boolean isSafe(int[][] m, int row,int col, boolean[][] visited){
        return (row >= 0) && (row < ROW) && (col >= 0) && (col < COL) && (m[row][col] == 1 && !visited[row][col]);
    }

    public int countIsland(int[][] m){
        boolean[][] visited = new boolean[ROW][COL];
        int count = 0;
        for (int i=0; i< ROW; i++){
            for (int j=0; j<COL; j++){
                if(!visited[i][j] && m[i][j] == 1){
                    DFS(m,i,j,visited);
                    count++;
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[][] m = new int[][] { { 1, 1, 0, 0, 0 },
                { 0, 1, 0, 0, 1 },
                { 1, 0, 0, 1, 1 },
                { 0, 0, 0, 0, 0 },
                { 1, 0, 1, 0, 1 } };
        Island island = new Island();
        System.out.println("no of islands " + island.countIsland(m));
    }
}
