package acrostic.serialize;

import java.io.Serializable;

public class TestSerialization implements Serializable {
    private  int a;
    private   String name;
    //transient and static members are not serialized
    public TestSerialization(int a, String name) {
        this.a = a;
        this.name = name;
    }

    public int getA() {
        return a;
    }

    public String getName() {
        return name;
    }
}
