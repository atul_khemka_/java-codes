package acrostic.serialize;

import java.io.*;

public class TestService {
    private TestSerialization testSerialization= new TestSerialization(2,"atul");
    private String filename = "file.ser";

    public TestService() {
    }

    public void serialize() throws IOException {
        FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(testSerialization);
        out.close();
        file.close();
        //testSerialization.name="adfs";
        System.out.println("serialization done");
    }

    public void deSerialize() throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file);
        TestSerialization newOb=null;
        newOb = (TestSerialization)in.readObject();
        in.close();
        file.close();
        System.out.println("deserialization done");
        System.out.println("a :="+ newOb.getA());
        System.out.println("name :="+ newOb.getName());
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TestService test = new TestService();
        test.serialize();
        test.deSerialize();
    }

}
