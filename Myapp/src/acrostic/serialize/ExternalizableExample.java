package acrostic.serialize;

import java.io.*;

public class ExternalizableExample {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Employee employee = new Employee("Atul","khemka",27,"SDE");
        System.out.println("object before serialization " + employee);
        serialize(employee);

        Employee deEmp = deSerialize();
        System.out.println("object after deserialization " + deEmp);
    }

    static void serialize(Employee employee) throws IOException {
        try (FileOutputStream fos = new FileOutputStream("data.obj");
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
                oos.writeObject(employee);
        }
    }

    static Employee deSerialize() throws IOException, ClassNotFoundException{
        try (FileInputStream fis = new FileInputStream("data.obj");
        ObjectInputStream ois = new ObjectInputStream(fis)){
            return (Employee)ois.readObject();
        }
    }
}
