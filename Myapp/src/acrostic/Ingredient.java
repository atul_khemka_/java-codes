package acrostic;

public class Ingredient implements Comparable<Ingredient>{
    private String name;
    private int priority;

    public Ingredient() {
    }

    public Ingredient(String name, int priority) {
        super();
        this.name = name;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "name='" + name + '\'' +
                ", priority=" + priority +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(Ingredient ingredient) {
        if(this.getPriority() > ingredient.getPriority()) return 1;
        else return -1;
    }
}
