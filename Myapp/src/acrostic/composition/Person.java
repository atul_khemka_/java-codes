package acrostic.composition;


// implement has-a relationship in classes , code reuse
// we can control the visibility of other object to client classes and reuse only what we need.
// situation where we have multiple levels of class inheritance and superclass is not controlled by us
// changes in superclass will impact subclass, this is controlled in composition
public class Person {
    private String name;
    private Job job;

    public Person(String name, long salary) {
        this.name = name;
        this.job = new Job();
        job.setSalary(salary);
    }

    public long getSalary(){
        return job.getSalary();
    }
}
