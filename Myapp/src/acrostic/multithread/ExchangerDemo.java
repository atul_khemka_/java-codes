package acrostic.multithread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

public class ExchangerDemo {
    private static Exchanger<DataBuffer> exchanger = new Exchanger<>();
    private static DataBuffer initialEmptyBuffer = new DataBuffer();
    private static DataBuffer initialFullBuffer = new DataBuffer("item");

    public static void main(String[] args) {

        class FillingLoop implements Runnable{
            int count = 0;

            @Override
            public void run() {
                DataBuffer currentBuffer = initialEmptyBuffer;
                try{
                    while (true){
                        addToBuffer(currentBuffer);
                        if(currentBuffer.isFull()){
                            System.out.println("filling loop thread wants to exchange");
                            currentBuffer = exchanger.exchange(currentBuffer);
                            System.out.println("filling loop thread observes an exchange");
                        }
                    }
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            void addToBuffer(DataBuffer buffer){
                String item = "NEWITEM"+count++;
                System.out.printf("Adding %s%n", item);
                buffer.add(item);
            }
        }

        class EmptyingLoop implements Runnable{

            @Override
            public void run() {
                DataBuffer currentBuffer = initialFullBuffer;
                try {
                    while (true){
                        takeFromBuffer(currentBuffer);
                        if(currentBuffer.isEmpty()){
                            System.out.println("emptying loop thread wants to exchange");
                            currentBuffer = exchanger.exchange(currentBuffer);
                            System.out.println("emptying loop thread observes an exchange");
                        }
                    }
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }

            void takeFromBuffer(DataBuffer buffer){
                System.out.printf("taking %s%n", buffer.remove());
            }
        }
        new Thread(new EmptyingLoop()).start();
        new Thread(new FillingLoop()).start();

    }
}

class DataBuffer{
    private final static int MAX = 10;
    private List<String> items;

    public DataBuffer() {
        items = new ArrayList<>();
    }

    public DataBuffer(String prefix) {
        items = new ArrayList<>();
        for(int i=0; i<MAX; i++)
            items.add(prefix+i);
    }
    public boolean isEmpty(){
        return items.size() == 0;
    }
    public boolean isFull(){
        return items.size() == MAX;
    }
    public void add(String s){
        if(!isFull())
            items.add(s);
    }
    public String remove(){
        if(!isEmpty())
            return items.remove(0);
        return null;
    }
}
