package acrostic.multithread;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ReadWebPage {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Callable<List<String>> callable;
        callable = () -> {
            List<String> lines = new ArrayList<>();
            URL url = new URL("http://www.anjecont.com/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String line;
                while ((line = br.readLine()) != null)
                    lines.add(line);
            }
            return lines;
        };

        Future<List<String>> future = executorService.submit(callable);
        try {
            List<String> lines = future.get(5, TimeUnit.SECONDS);
            for(String line: lines)
                System.out.println(line);
        }
        catch (ExecutionException ee){
            System.err.println("Callable through exception: "+ee.getMessage());
        }
        catch (InterruptedException | TimeoutException e) {
            System.err.println("URL not responding");
        }
        executorService.shutdown();

    }
}
