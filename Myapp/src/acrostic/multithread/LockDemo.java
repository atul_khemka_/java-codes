package acrostic.multithread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo {

    public static void main(String[] args) {

        final ReentrantLock lock = new ReentrantLock();

        class Worker implements Runnable{
            private String name;

            public Worker(String name) {
                this.name = name;
            }

            @Override
            public void run() {
                lock.lock();
                try{
                    if(lock.isHeldByCurrentThread())
                        System.out.printf("Worker %s has entered critical section%n",name);
                    Thread.sleep(2000);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    System.out.printf("Thread %s has finished working.%n", name);
                    lock.unlock();
                }
            }
        }
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new Worker("A"));
        executorService.execute(new Worker("B"));
        try{
            executorService.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e){

        }
        executorService.shutdownNow();
    }
}
