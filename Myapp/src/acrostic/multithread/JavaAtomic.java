package acrostic.multithread;

import java.util.concurrent.atomic.AtomicInteger;

public class JavaAtomic {

    public static void main(String[] args) throws InterruptedException{
        ProcessThread pt = new ProcessThread();
        Thread t1 = new Thread(pt, "t1");
        t1.start();
        Thread t2 = new Thread(pt, "t2");
        t2.start();
        t1.join();
        t2.join();
        System.out.println(pt.getCount());
    }

}

class ProcessThread implements Runnable{
     private AtomicInteger count = new AtomicInteger();

    @Override
    public void run() {
        for (int i=1;i<5;i++){
            doSomething(i);
            count.incrementAndGet();
        }
    }

    private void doSomething(int i){
        try{
            Thread.sleep(i*1000);
        }
        catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public int getCount(){
        return this.count.get();
    }
}
