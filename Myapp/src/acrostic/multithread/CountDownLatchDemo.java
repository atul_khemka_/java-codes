package acrostic.multithread;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch startSignal = new CountDownLatch(1);
        CountDownLatch doneSignal = new CountDownLatch(3);
        for(int i=0; i<3;i++){
            new Thread(new Worker(startSignal,doneSignal)).start();
        }
        System.out.println("Letting threads proceed");
        startSignal.countDown();
        System.out.println("waiting for threads to finish ");
        doneSignal.await();
        System.out.println("main thread done");
    }
}

class Worker implements Runnable{
    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;

    public Worker(CountDownLatch startSignal, CountDownLatch doneSignal) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        try {
            startSignal.await();
            for(int i=0; i< 4; i++){
                System.out.printf("thread %s is working%n",name);
                Thread.sleep((int)(Math.random()*300));
            }
            System.out.printf("Thread %s finishing%n",name);
            doneSignal.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
