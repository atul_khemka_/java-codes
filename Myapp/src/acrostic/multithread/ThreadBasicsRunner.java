package acrostic.multithread;

class Task1 extends Thread{
    public void run(){
        System.out.print("\nTask1 Started");

        for(int i=101;i<=199; i++)
            System.out.print(i + " ");

        System.out.print("\nTask1 Done");
    }
}
class Task2 implements Runnable{

    @Override
    public void run() {
        System.out.print("\nTask2 Started");
        for(int i=200;i<299;i++)
            System.out.println(i + " ");
        System.out.print("\nTask2 done");
    }
}
public class ThreadBasicsRunner {
    public static void main(String[] args) throws InterruptedException {
        System.out.print("\nTask1 Kicked Off");
        Task1 task1 = new Task1();
        task1.setPriority(1);
        task1.start();

        System.out.print("\nTask2 Kicked Off");
        Thread task2 = new Thread(new Task2());
        task2.setPriority(10);
        task2.start();

        task1.join();
        task2.join();
        System.out.print("\nTask3 Kicked Off");

        //Task3
        for(int i=301;i<=399; i++)
            System.out.print(i + " ");

        System.out.print("\nTask3 Done");

        System.out.print("\nMain Done");
    }
}
