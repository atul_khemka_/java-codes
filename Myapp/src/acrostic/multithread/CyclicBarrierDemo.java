package acrostic.multithread;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierDemo {

    public static void main(String[] args) {

        Runnable action  = () ->{
          String name = Thread.currentThread().getName();
            System.out.printf("Thread %s executing barrier action%n", name);
        };

        final CyclicBarrier barrier = new CyclicBarrier(3,action);
        Runnable task = () -> {
          String name = Thread.currentThread().getName();
            System.out.printf("%s about to join  the game ..%n", name);
            try {
                barrier.await();
            }
            catch (BrokenBarrierException e) {
                System.out.println("barrier is broken");
                return;
            }
             catch (InterruptedException e) {
                System.out.println("thread interrupted");
                return;
            }
            System.out.printf("%s has joined game%n", name);
        };

        ExecutorService[] exeutors = new ExecutorService[]{
                Executors.newSingleThreadExecutor(),
                Executors.newSingleThreadExecutor(),
                Executors.newSingleThreadExecutor() };
        for(ExecutorService executor: exeutors){
            executor.execute(task);
            executor.shutdown();
        }
    }
}
