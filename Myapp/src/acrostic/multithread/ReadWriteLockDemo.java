package acrostic.multithread;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLockDemo {
    final static int DELAY = 80;
    final static int NUMITER = 5;

    public static void main(String[] args) {
        Names names = new Names();

        class NamedThread implements ThreadFactory{
            private String name;

            public NamedThread(String name) {
                this.name = name;
            }

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, name);
            }
        }

        ExecutorService writer = Executors.newSingleThreadExecutor(new NamedThread("writer"));
        Runnable wRunnable = () -> {
          for(int i=0; i<NUMITER;i++){
              names.add(Thread.currentThread().getName(),"A"+i);
              try {
                  Thread.sleep(DELAY);
              }
              catch (InterruptedException e){
                  e.printStackTrace();
              }
          }
        };
        writer.submit(wRunnable);
        ExecutorService reader1 = Executors.newSingleThreadExecutor(new NamedThread("reader1"));
        ExecutorService reader2 = Executors.newSingleThreadExecutor(new NamedThread("reader2"));
        Runnable rRunnable = () ->{
          for (int i=0; i<NUMITER; i++){
              names.dump(Thread.currentThread().getName());
          }
        };

        reader1.submit(rRunnable);
        reader2.submit(rRunnable);

        reader1.shutdown();
        reader2.shutdown();
        writer.shutdown();
    }
}

class Names{
    private final List<String> names;
    private final ReentrantReadWriteLock lock;
    private final Lock readLock, writeLock;

    Names(){
        names = new ArrayList<>();
        lock = new ReentrantReadWriteLock();
        readLock = lock.readLock();
        writeLock = lock.writeLock();
    }

    void add(String thread, String name){
        writeLock.lock();
        try{
            System.out.printf("%s: num waiting threads =%d%n", thread, lock.getQueueLength());
            names.add(name);
        }
        finally {
            writeLock.unlock();
        }
    }
    void dump(String thread){
        readLock.lock();
        try {
            System.out.printf("%s: num waiting threads = %d%n", thread, lock.getQueueLength());
            Iterator<String> it = names.iterator();
            while(it.hasNext()){
                System.out.printf("%s: %s%n", thread, it.next());
                try {
                    Thread.sleep((int)(Math.random()*100));
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
        finally {
            readLock.unlock();
        }
    }
}
