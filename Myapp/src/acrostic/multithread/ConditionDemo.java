package acrostic.multithread;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConditionDemo {
    public static void main(String[] args) {
        Shared shared = new Shared();
        new Producer(shared).start();
        new Consumer(shared).start();
    }
}

class Shared{
    private volatile char c;
    private volatile boolean available;
    private final Lock lock;
    private final Condition condition;

    Shared(){
        c = '\u0000';
        available = false;
        lock = new ReentrantLock();
        condition = lock.newCondition();
    }

    public Lock getLock(){
        return lock;
    }

    char getSharedChar(){
        lock.lock();
        try {
            while (!available){
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            available = false;
            condition.signal();
        }
        finally {
            lock.unlock();
        }
        return c;
    }

    void setSharedChar(char c){
        lock.lock();
        try {
            while (available){
                try {
                    condition.await();
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            this.c = c;
            available = true;
            condition.signal();
        }
        finally {
            lock.unlock();
        }
    }

}

class Producer extends Thread{

    private final Lock l;
    private final Shared shared;
    Producer(Shared s){
        shared = s;
        l =s.getLock();
    }

    @Override
    public void run() {
        for( char ch = 'A'; ch<= 'Z'; ch++){
            l.lock();
            shared.setSharedChar(ch);
            System.out.println(ch+ " produced by producer");
            l.unlock();
        }
    }
}

class Consumer extends Thread{
    private final Lock l;
    private final Shared shared;
    Consumer(Shared s){
        shared = s;
        l =s.getLock();
    }

    @Override
    public void run() {
        char ch;
        do{
            l.lock();
            ch = shared.getSharedChar();
            System.out.println(ch+ " consumed by Consumer");
            l.unlock();
        }
        while (ch != 'Z');
    }
}
