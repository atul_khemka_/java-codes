package acrostic.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericRunner {

    static <X> X func(X value){
        return value;
    }
    static <X extends List> void func1(X list){
        list.addAll(list);
    }
    static double addlist(List<? extends Number> numbers){
        double sum = 0.0;
        for (Number number: numbers)
            sum += number.doubleValue();
        return sum;
    }

    public static void main(String[] args) {
        /*
        MyCustomList<String> list = new MyCustomList<>();
        list.addElement("x1");
        System.out.println(list.get(0));*/
        MyCustomList<Long> list = new MyCustomList<>();
        list.addElement(5l);
        System.out.println(list.get(0));
        MyCustomList<Integer> list2 = new MyCustomList<>();
        list2.addElement(Integer.valueOf(5));
        System.out.println(list2.get(0));
        func("sdf");
        ArrayList<Integer> list3 = new ArrayList<>(List.of(1,2,3,4));
        func1(list3);
        System.out.println(list3);
        System.out.println(addlist(List.of(1,2,3)));
    }
}
