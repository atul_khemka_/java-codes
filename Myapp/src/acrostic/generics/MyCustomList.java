package acrostic.generics;

import java.util.ArrayList;

//restricts the Type T to number , methods of parent class can be used
public class MyCustomList<T extends Number> {
    ArrayList<T> list = new ArrayList<>();
    public void addElement(T x){
        list.add(x);
    }
    public void removeElement(T x){
        list.remove(x);
    }
    public T get(int pos){
        return list.get(pos);
    }
}
/*
public class MyCustomList<T> {
    ArrayList<T> list = new ArrayList<>();
    public void addElement(T x){
        list.add(x);
    }
    public void removeElement(T x){
        list.remove(x);
    }
    public T get(int pos){
        return list.get(pos);
    }
}

 */
