package acrostic.compare;

import java.util.*;

public class Runner {
    public static void main(String[] args) {
        List<Laptop> laptops= new ArrayList<>();
        laptops.add(new Laptop("dell",4,20000));
        laptops.add(new Laptop("asuc",3,12345));
        laptops.add(new Laptop("apple",4,40000));

        Collections.sort(laptops);
        for(Laptop l: laptops)
            System.out.println(l);
        Comparator<Laptop> comparator = (o1,  o2) -> o1.getPrice()>o2.getPrice()?1:-1;

        Collections.sort(laptops, comparator);
        for(Laptop l: laptops)
            System.out.println(l);
        System.out.println("loop test");
        laptops.forEach(System.out::println);
    }
}
