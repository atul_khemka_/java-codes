package practice;

import java.util.*;

class Node{
    int key;
    Node left, right;

    public Node(int key) {
        this.key = key;
        left = right = null;
    }
}

public class BinaryTree {
    Node root;

    public BinaryTree(Node root) {
        this.root = root;
    }

    public void bottomView(){
        Map<Integer, Pair<Integer,Integer>> map = new TreeMap<>();
        bottomViewUtil(root,0,0,map);

        for ( Pair<Integer, Integer> pair: map.values())
            System.out.print(pair.getFirst()+" ");
    }

    private void bottomViewUtil(Node root, int curr, int hd, Map<Integer, Pair<Integer, Integer>> map){
        if (root == null)
            return;

        if(!map.containsKey(hd)) {
            map.put(hd, new Pair<>(root.key, curr));
        }
        else{
            Pair<Integer, Integer> pair = map.get(hd);
            if(pair.getSecond() <= curr){
                pair.setSecond(curr);
                pair.setFirst(root.key);
            }
        }
        bottomViewUtil(root.left, curr+1,hd-1,map);
        bottomViewUtil(root.right, curr+1,hd+1,map);
    }

    public void ZigZagView(){
        if(root == null)
            return;
        //two stacks are used
        Deque<Node> currLevel = new LinkedList<>();
        Deque<Node> nextLevel = new LinkedList<>();
        boolean leftToRight = true;
        currLevel.push(root);

        while (!currLevel.isEmpty()){
            Node temp = currLevel.pop();
            System.out.print(temp.key+" ");
            if(leftToRight){
                if(temp.left != null)
                    nextLevel.push(temp.left);
                if(temp.right != null)
                    nextLevel.push(temp.right);
            }
            else{
                if(temp.right != null)
                    nextLevel.push(temp.right);
                if(temp.left != null)
                    nextLevel.push(temp.left);
            }
            if(currLevel.isEmpty()){
                leftToRight = !leftToRight;
                Deque<Node> tmp = currLevel;
                currLevel = nextLevel;
                nextLevel = tmp;
            }
        }
    }

    public void reverseLevelOrder(){
        if (root == null)
            return;
        Queue<Node> queue = new LinkedList<>();
        Deque<Node> stack = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            Node temp = queue.poll();
            stack.push(temp);
            if(temp.right != null)
                queue.add(temp.right);
            if(temp.left != null)
                queue.add(temp.left);
        }
        while (!stack.isEmpty()){
            System.out.print(stack.pop().key+" ");
        }
    }

    public static void main(String[] args) {
        Node root = new Node(20);
        root.left = new Node(8);
        root.right = new Node(22);
        root.left.left = new Node(5);
        root.left.right = new Node(3);
        root.right.left = new Node(4);
        root.right.right = new Node(25);
        root.left.right.left = new Node(10);
        root.left.right.right = new Node(14);
        BinaryTree tree = new BinaryTree(root);
        tree.bottomView();
        System.out.println();
        tree.ZigZagView();
        System.out.println();
        tree.reverseLevelOrder();
    }
}
