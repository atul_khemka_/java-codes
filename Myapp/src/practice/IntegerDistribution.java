package practice;

import java.io.*;
import java.util.*;

public class IntegerDistribution {
    public static void main(String[] args) throws IOException {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
          PrintWriter wr = new PrintWriter(System.out);
//        int n = Integer.parseInt(br.readLine().trim());
//
//        int[] Arr = new int[n];
//        for (int i_Arr = 0; i_Arr < n; i_Arr++) {
//            String arr_Arr = br.readLine().trim();
//            Arr[i_Arr] = Integer.parseInt(arr_Arr);
//        }
        int[] Arr = {1,2,3,4,5,6};
        long[] out_ = Min_and_Max(Arr);
        wr.print(out_[0]);
        wr.print(out_[1]);
        wr.close();
       // br.close();
    }

    static long[] Min_and_Max(int[] Arr) {
        Arrays.sort(Arr);
        long[] output = new long[2];
        Map<String, List<Long>> minMaxMap = new HashMap<>();
        List<Long> solution = solve(Arr, minMaxMap);
        output[0] = solution.get(0);
        output[1] = solution.get(1);
        return output;
    }

    static List<Long> solve(int[] Arr, Map<String, List<Long>> minMaxMap) {
        String key = getDPKey(Arr);
        if(Arr.length == 2) {
            long solutionVal = Arr[0] ^ Arr[1];
            return new ArrayList<Long>(Arrays.asList(solutionVal, solutionVal));
        } else if(minMaxMap.containsKey(key)) {
//            System.out.println("Cached");
            return minMaxMap.get(key);
        }

        long totalMin = Long.MAX_VALUE;
        long totalMax = Long.MIN_VALUE;

        for(int j=1; j<Arr.length; j++) {
            int[] subArr = getSubArr(0, j, Arr);
            List<Long> subSolution = solve(subArr, minMaxMap);
            int xor = (Arr[0] ^ Arr[j]);
            long maxSum = xor + subSolution.get(1);
            long minSum = xor + subSolution.get(0);

            if(maxSum > totalMax) totalMax = maxSum;
            if(minSum < totalMin) totalMin = minSum;
        }

        List<Long> currentSolution = new ArrayList<Long>();
        currentSolution.add(totalMin);
        currentSolution.add(totalMax);
        minMaxMap.put(key, currentSolution);
        return currentSolution;
    }

    static int[] getSubArr(int i, int j, int[] Arr) {
        List<Integer> subList = new ArrayList<>();
        for(int k=0; k< Arr.length; k++) {
            if(k == i || k ==j) continue;
            subList.add(Arr[k]);
        }
        return subList.stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }

    static String getDPKey(int[] Arr) {
        String key = "";
        for (int value : Arr) {
            key += value + ",";
        }
        return key;
    }

}
