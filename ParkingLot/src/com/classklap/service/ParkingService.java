package com.classklap.service;

import com.classklap.repo.DataManager;
import com.classklap.model.Vehicle;
import java.util.List;

public class ParkingService {
    DataManager dataManager = null;

    public void createParkingLot(int capacity) throws Exception {
        if (dataManager != null) throw new Exception("Parking lot already exits");
        dataManager = DataManager.getInstance(capacity);
        System.out.println("Created a parking lot with "+ capacity + " slots");
    }

    public int park(Vehicle vehicle) throws Exception {
        lotExists();
        int value = 0;
        try
        {
            value = dataManager.parkCar(vehicle);
            if (value == -1)
                System.out.println("Sorry, parking lot is full");
            else if (value == -2)
                System.out.println("Vehicle is already parked.");
            else
                System.out.println("Allocated slot number: " + value);
        }
        catch (Exception e)
        {
            System.out.println("Something went wrong");
        }

        return value;
    }

    private void lotExists() throws Exception
    {
        if (dataManager == null)
        {
            throw new Exception("Car Parking Does not Exist");
        }
    }

    public void unPark(int slotNumber) throws Exception {
        lotExists();
        try{
            if(dataManager.leaveCar(slotNumber))
                System.out.println("Slot number " + slotNumber + " is free");
            else
                System.out.println("Slot number is Empty Already.");
        }
        catch (Exception e){
            System.out.println("Something went wrong");
        }
    }

    public void getStatus() throws Exception {
        lotExists();
        try{
            System.out.println("Slot no. Registration no.\t Color");
            List<String> status = dataManager.getStatus();
            if (status.size() == 0)
                System.out.println("Empty Lot");
            else{
                for(String statusline : status)
                    System.out.println(statusline);
            }
        }
        catch (Exception e){
            System.out.println("Something went wrong");
        }
    }

    public void getRegNumberForColor(String color) throws Exception {
        lotExists();
        try{
            List<String> regList = dataManager.getRegNumberForColor(color);
            if(regList.size() == 0)
                System.out.println("Car not found");
            else
                System.out.println(String.join(", ", regList));
        }
        catch (Exception e){
            System.out.println("Something went wrong");
        }
    }

    public void getSlotNumbersFromColor(String color) throws Exception {
        lotExists();
        try{
            List<String> slots = dataManager.getSlotNumbersFromColor(color);
            if(slots.size() == 0)
                System.out.println("Car not found");
            else
                System.out.println(String.join(", ", slots));
        }
        catch (Exception e){
            System.out.println("Something went wrong");
        }
    }

    public int getSlotNoFromRegistrationNo(String registrationNo) throws Exception {
        lotExists();
        int slot = -1;
        try {
            slot = dataManager.getSlotNoFromRegistrationNo(registrationNo);
            if(slot == -1) System.out.println("Car not found");
            else System.out.println(slot);
        }
        catch (Exception e){
            System.out.println("Something went wrong");
        }
        return slot;
    }

}
