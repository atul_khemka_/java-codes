package com.classklap.service;

import com.classklap.util.InputCommands;

public class InputValidate {
    public  boolean validate(String input)
    {
        boolean valid = true;
        try
        {
            String[] inputs = input.split(" ");
            int params = InputCommands.getCommands().get(inputs[0]);
            switch (inputs.length)
            {
                case 1:
                    if (params != 0)
                        valid = false;
                    break;
                case 2:
                    if (params != 1)
                        valid = false;
                    break;
                case 3:
                    if (params != 2)
                        valid = false;
                    break;
                default:
                    valid = false;
            }
        }
        catch (Exception e)
        {
            valid = false;
        }
        return valid;
    }
}
