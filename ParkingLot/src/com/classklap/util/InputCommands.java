package com.classklap.util;

import java.util.HashMap;
import java.util.Map;

public class InputCommands {
    private static  Map<String, Integer> commands = new HashMap<>();

    static
    {
        commands.put("create_parking_lot", 1);
        commands.put("park", 2);
        commands.put("leave", 1);
        commands.put("status", 0);
        commands.put("registration_numbers_for_cars_with_colour", 1);
        commands.put("slot_numbers_for_cars_with_colour", 1);
        commands.put("slot_number_for_registration_number", 1);
    }

    public static Map<String, Integer> getCommands()
    {
        return commands;
    }
}
