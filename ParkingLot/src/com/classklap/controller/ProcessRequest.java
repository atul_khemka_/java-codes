package com.classklap.controller;

import com.classklap.model.Car;
import com.classklap.service.ParkingService;

public class ProcessRequest {
    ParkingService parkingService = new ParkingService();
    public void process(String input) throws Exception {
        String[] inputs = input.split(" ");
        String command = inputs[0];
        switch (command)
        {
            case "create_parking_lot":
                try {
                    int capacity = Integer.parseInt(inputs[1]);
                    parkingService.createParkingLot(capacity);
                }
                catch (NumberFormatException e){
                    System.out.println(" Enter valid number of {capacity}");
                }
                break;

            case "park":
                parkingService.park(new Car(inputs[1],inputs[2]));
                break;

            case "leave":
                try {
                    int slot_number = Integer.parseInt(inputs[1]);
                    parkingService.unPark(slot_number);
                }
                catch (NumberFormatException e){
                    System.out.println(" Enter valid slot no ");
                }
                break;

            case "status":
                parkingService.getStatus();
                break;

            case "registration_numbers_for_cars_with_colour":
                parkingService.getRegNumberForColor(inputs[1]);
                break;

            case "slot_numbers_for_cars_with_colour":
                parkingService.getSlotNumbersFromColor(inputs[1]);
                break;

            case "slot_number_for_registration_number":
                parkingService.getSlotNoFromRegistrationNo(inputs[1]);
                break;

            default:
                break;
        }
    }
}
