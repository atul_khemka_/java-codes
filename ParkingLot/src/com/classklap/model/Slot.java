package com.classklap.model;

import java.util.TreeSet;

public class Slot {

    private TreeSet<Integer> freeSlots;

	public Slot() {
	    freeSlots = new TreeSet<Integer>();
	}

    public void add(int i)
    {
        freeSlots.add(i);
    }

    public int getSlot()
    {
        return freeSlots.first();
    }

    public void removeSlot(int availableSlot)
    {
        freeSlots.remove(availableSlot);
    }

}
