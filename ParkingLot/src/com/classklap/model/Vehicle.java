package com.classklap.model;

import java.util.Objects;

public class Vehicle {
    private String regNo = null;
    private String color = null;

    public Vehicle(String regNo, String color) {
        this.regNo = regNo;
        this.color = color;
    }

    public String getRegNo() {
        return regNo;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return getRegNo().equalsIgnoreCase(vehicle.getRegNo());
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "regNo='" + regNo + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
