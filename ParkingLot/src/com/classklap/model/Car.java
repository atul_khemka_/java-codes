package com.classklap.model;

public class Car extends Vehicle {
    public Car(String regNo, String color) {
        super(regNo, color);
    }
}
