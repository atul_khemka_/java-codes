package com.classklap;

import com.classklap.controller.ProcessRequest;
import com.classklap.service.InputValidate;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
	// write your code here
        BufferedReader bufferReader = null;
        InputValidate inputValidate = new InputValidate();
        ProcessRequest processRequest = new ProcessRequest();
        String input = null;
        System.out.println("--------------------My Parking Lot---------------------");
        Info();
        while (true) {
            try {
                bufferReader = new BufferedReader(new InputStreamReader(System.in));
                input = bufferReader.readLine().trim();
                if (inputValidate.validate(input)) {
                    try {
                        processRequest.process(input);
                    }
                    catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                } else {
                    System.out.println("\nIncorrect inputs\n");
                    Info();
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void Info(){
        StringBuilder built = new StringBuilder();
        built = built.append("Please Input one of the commands").append("\n");
        built = built.append("1.Built a parking lot of capacity n   ==> create_parking_lot {capacity}").append("\n");
        built = built.append("2.To park Car   ==> park {vehicle no} {color}").append("\n");
        built = built.append("3.Leave the lot   ==> leave {slot_no}").append("\n");
        built = built.append("4.Current Status of parking lot   ==> status").append("\n");
        built = built.append("5.Get registration no for the given car color  ==> registration_numbers_for_cars_with_colour {color}").append("\n");
        built = built.append("6.Get slot numbers for the given car color   ==> slot_numbers_for_cars_with_colour {color}").append("\n");
        built = built.append("7.Get slot number for the given car number   ==> slot_number_for_registration_number {vehicle no}").append("\n");
        System.out.println(built);
    }
}
