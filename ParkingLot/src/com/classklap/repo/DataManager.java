package com.classklap.repo;

import com.classklap.model.Slot;
import com.classklap.model.Vehicle;
import java.util.*;

public class DataManager {
    private final int capacity;
    private  int availability;
    private Slot slot;

    private Map<Integer, Vehicle> parkingLot;
    private static DataManager instance = null;

    public static DataManager getInstance(int capacity)
    {
        if (instance == null)
        {
            instance = new DataManager(capacity);

        }
        return instance;
    }

    private DataManager(int capacity)
    {

        this.capacity = capacity;
        this.availability = capacity;
        if (slot == null)
            slot = new Slot();
        parkingLot = new HashMap<>();
        for (int i = 1; i <= capacity; i++)
        {
            parkingLot.put(i, null);
            slot.add(i);
        }
    }

    public int parkCar(Vehicle vehicle)
    {
        int freeSlot;
        if (this.availability == 0)
            return -1;
        else
        {
            freeSlot = slot.getSlot();
            if (parkingLot.containsValue(vehicle))
                return -2;

            parkingLot.put(freeSlot, vehicle);
            availability--;
            slot.removeSlot(freeSlot);
        }
        return freeSlot;
    }

    public boolean leaveCar(int slotNumber)
    {
        if (parkingLot.get(slotNumber) == null)
            return false;
        availability++;
        slot.add(slotNumber);
        parkingLot.put(slotNumber, null);
        return true;
    }

    public List<String> getStatus()
    {
        List<String> status = new ArrayList<>();
        for (int i = 1; i <= capacity; i++)
        {
            Vehicle vehicle = parkingLot.get(i);
            if (vehicle != null)
            {
                status.add(i + "\t\t  " + vehicle.getRegNo() + "\t\t  " + vehicle.getColor());
            }
        }
        return status;
    }

    public List<String> getRegNumberForColor(String color){
        List<String> reg = new ArrayList<>();

        for (int i = 1; i <= capacity; i++)
        {
            Vehicle vehicle = parkingLot.get(i);
            if (vehicle != null && color.equalsIgnoreCase(vehicle.getColor()))
            {
                reg.add(vehicle.getRegNo());
            }
        }
        return reg;
    }

    public List<String> getSlotNumbersFromColor(String color){
        List<String> reg = new ArrayList<>();

        for (int i = 1; i <= capacity; i++)
        {
            Vehicle vehicle = parkingLot.get(i);
            if (vehicle != null && color.equalsIgnoreCase(vehicle.getColor()))
                reg.add(Integer.toString(i));
        }
        return reg;
    }

    public int getSlotNoFromRegistrationNo(String regNo){
        int slots = -1;
        for (int i = 1; i <= capacity; i++) {
            Vehicle vehicle = parkingLot.get(i);
            if (vehicle != null && regNo.equalsIgnoreCase(vehicle.getRegNo()))
                slots = i;
        }
        return slots;
    }

}
